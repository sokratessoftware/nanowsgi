NANOWSGI
========

This is a minimal WSGI framework that I accidentally wrote while working on some file serving applications.

What it aims to provide:

  * unicode inputs in both Python 2 and 3
  * zero-copy file uploads
  * freedom from magic globals
  * a convenient Request class
  * ability to return HTTP errors using exceptions

What it doesn't provide:

  * URL routing
  * database access
  * support for encodings other than UTF-8

EXAMPLE
-------

    import nanowsgi
    
    def my_application (rq):
        if rq.path_info == "/fail":
            raise rq.fail (400)
        
        return rq.respond_with_text ("OK")
    
    application = nanowsgi.make_wsgi_application (my_application)
