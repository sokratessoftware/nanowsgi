# coding=utf-8
from __future__ import unicode_literals

import sys
import traceback

from .exceptions import ProgrammingError
from .utils import header_dict_to_list, reset_harakiri

__all__ = ["make_wsgi_application", "ProgrammingError"]

def make_wsgi_application (request_callback,
                           exception_callback=None,
                           trust_forwarded_headers=False,
                           http_port_override=None,
                           https_port_override=None,
                           _reset_harakiri=reset_harakiri,
                           _Request=None,
                           _Response=None,
                           _ErrorResponse=None):
    """
    Create a WSGI application callable.
    
    :param request_callback:   A callable expecting a nanowsgi.Request object for handling requests.
    :param exception_callback: A callable expecting a nanowsgi.Request object for handling exceptions.
    :return:                   WSGI callable
    """
    
    from . import response
    from . import request
    _Request = _Request or request.Request
    _Response = _Response or response.Response
    _ErrorResponse = _ErrorResponse or response.ErrorResponse
    
    def nanowsgi_wrapper (env, start_response,
                          request_callback=request_callback,
                          exception_callback=exception_callback,
                          _Request=_Request,
                          _Exception=Exception,
                          _500=response.prep_status_line (500),
                          _ErrorResponse=_ErrorResponse):
        rq = None
        loop = 1
        
        while 1:
            # looping to reuse the same exception handler to call the exception callback
            
            try:
                rq = rq or _Request (env,
                                     trust_forwarded_headers=trust_forwarded_headers,
                                     http_port_override=http_port_override,
                                     https_port_override=https_port_override,
                                     _ErrorResponse=_ErrorResponse)
                
                rs = request_callback (rq) or rq.response or _Response (204)
                if not rq._body_read:
                    rq.discard_body ()
                
                _reset_harakiri(0)
                return rs._send (start_response)
            
            except _ErrorResponse as error:
                if rq and not rq._body_read:
                    rq.discard_body ()

                _reset_harakiri(0)
                return error._send (start_response)
            
            except _Exception as ex:
                if loop and exception_callback and rq:
                    loop = 0
                    request_callback = exception_callback
                    rq.exception = ex
                    rq.exc_info = sys.exc_info ()
                    continue
                
                else:
                    to = env.get ("wsgi.errors", sys.stderr)
                    
                    if rq and rq.exc_info:
                        # exception handling callback crashed... let's document the original exception it was handling
                        traceback.print_exception (*rq.exc_info, file=to)
                    
                    traceback.print_exc (file=to)
                
                if rq and not rq._body_read:
                    rq.discard_body ()
                
                _reset_harakiri(0)
                start_response (_500, header_dict_to_list ({ "CONTENT-TYPE": "text/plain", "CONTENT-LENGTH": "0" }))
                return ()
    
    return nanowsgi_wrapper
