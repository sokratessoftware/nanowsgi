# coding=utf-8
from __future__ import unicode_literals

import os, shutil, tempfile
import cgi

import logging

from . import utils

def file_iter (fileobj, chunk_size=16384, size=None, _fstat=os.fstat):
    """
    Basic chunked file iterator, used in case the WSGI server doesn't provide its own.
    """
    
    fileobj.seek (0)
    if size is None:
        size = _fstat (fileobj.fileno ()).st_size
    
    _min = min
    _len = len
    while size > 0:
        chunk = fileobj.read (_min (size, chunk_size))
        
        if not chunk:
            break
        
        size -= _len (chunk)
        yield chunk
    
    try:
        fileobj.close ()
    except OSError:
        pass

class TemporaryFileWithChecksum(object):
    checksum = None
    __file = None
    __harakiri_counter = 64*1024
    
    def __init__(self, *args, **kwargs):
        checksum = self.hash_algorithm = kwargs.pop("hash_algorithm", None)
        if checksum:
            self.checksum = checksum()
        
        self.__file = tempfile.NamedTemporaryFile(*args, **kwargs)
    
    def write(self, data,
              _len=len,
              _reset_harakiri=utils.reset_harakiri):
        if self.checksum:
            self.checksum.update(data)

        self.__harakiri_counter -= _len(data)
        if self.__harakiri_counter <= 0:
            # co 64KB resetujemy timeout wykrywacza zwiech
            self.__harakiri_counter = 64*1024
            _reset_harakiri()
        
        return self.__file.write(data)
    
    def __getattr__(self, key, _getattr=getattr):
        return _getattr(self.__file, key)

class FieldStorage (cgi.FieldStorage):
    """
    cgi.FieldStorage descendant with support for:
    
    * specifying the directory to temporarily store uploaded files
    * ~~controlling the maximum size of accepted files before storing them~~
    * calculating a checksum using hashlib
    * using filesystem-visible temporary files that can be atomically moved
    """
    
    __checksum = None
    
    def __init__ (self, *args, **kwargs):
        self.temporary_directory = kwargs.pop ("temporary_directory", None)
        self.hash_algorithm = kwargs.pop ("hash_algorithm", None)
        kwargs.pop("max_memory_size", None) # not supported anymore
        cgi.FieldStorage.__init__ (self, *args, **kwargs)
    
    def FieldStorageClass (self, *args, **kwargs):
        kwargs["temporary_directory"] = self.temporary_directory
        kwargs["hash_algorithm"]      = self.hash_algorithm
        return self.__class__ (*args, **kwargs)
    
    def make_file (self, binary=None, size=None,
                   _TemporaryFile=TemporaryFileWithChecksum,
                   _log=logging.getLogger("nanowsgi.FieldStorage.make_file")):
        if self._binary_file:
            return _TemporaryFile("wb+",
                                  hash_algorithm=self.hash_algorithm,
                                  prefix="nanowsgi_upload_",
                                  dir=self.temporary_directory)
        else:
            _log.warning("Handling output as text file with encoding: " + self.encoding)
            return _TemporaryFile("w+",
                                  hash_algorithm=self.hash_algorithm,
                                  prefix="nanowsgi_upload_",
                                  dir=self.temporary_directory,
                                  encoding=self.encoding,
                                  newline="\n")
    
    def get_actual_length(self):
        file = self.file
        pos = file.tell()
        file.seek(0, 2)
        val = file.tell()
        file.seek(pos)
        return val
    
    def get_checksum(self,
                     _getattr=getattr,
                     _log=logging.getLogger("nanowsgi.FieldStorage.get_checksum")):
        checksum = self.__checksum
        if checksum is None:
            checksum = _getattr(self.file, "checksum", None)
            if checksum is None:
                if self.hash_algorithm:
                    checksum = self.hash_algorithm()
                    contents = self.file.getvalue()
                    if type(contents) is str:
                        _log.warning("Calculating checksum for string-like resource with encoding: " + self.encoding)
                        contents = contents.encode(self.encoding, self.errors)
                    checksum.update(contents)
            self.__checksum = checksum
        return checksum

class UploadedFile (object):
    """
    Wrapper for uploaded files.
    """
    
    file = None
    """The file object - either a NamedTemporaryFile or a StringIO/BytesIO instance."""
    
    name = None
    """Field name in the uploaded form."""
    
    filename = None
    """Unicode filename given by uploader."""
    
    temporary_filename = None
    """Path to temporary file or None if buffered in memory."""
    
    content_type = None
    content_length = None
    
    checksum = None
    """Calculated checksum if one was requested, None otherwise."""
    
    def __init__ (self, fileobj, name, filename=None, content_type=None, content_length=None, checksum=None, temporary_filename=None):
        self.file = fileobj
        self.name = name
        self.filename = filename
        self.content_type = content_type
        self.content_length = content_length
        self.checksum = checksum
        self.temporary_filename = temporary_filename
    
    def save (self, path, mode=None, temp_dir=None,
              _chmod=os.chmod,
              _move=shutil.move,
              _dirname=os.path.dirname,
              _getattr=getattr,
              _TemporaryFile=tempfile.NamedTemporaryFile):
        """
        Save the file to a permanent location. If possible, using an atomic rename.
        """
        
        if self.temporary_filename:
            if mode is not None:
                _chmod (self.temporary_filename, mode)
            _move (self.temporary_filename, path)
            self.file.delete = False
            closer = _getattr (self.file, "_closer", None) # stupid python 3
            if closer:
                closer.delete = False
        
        else:
            with _TemporaryFile (dir=temp_dir or _dirname (path), prefix="nanowsgi_save_") as tmp:
                tmp.write (self.file.getvalue ())
                if mode is not None:
                    _chmod (tmp.name, mode)
                _move (tmp.name, path)
                tmp.delete = False
                closer = _getattr (tmp, "_closer", None)
                if closer:
                    closer.delete = False
