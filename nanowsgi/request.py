# coding=utf-8
from __future__ import unicode_literals

import codecs
import collections
import json
import operator

import six
from six.moves.urllib import parse as urlparse

from . import utils, files as files_module, response as response_module, ProgrammingError

__all__ = ["Request"]

class Request (object):
    response = None
    exception = None
    exc_info = None
    
    headers = ()
    
    _body_read = False
    _posted_data = None
    _posted_data_list = None
    _posted_files = None
    
    def __init__ (self, env,
                  trust_forwarded_headers=False,
                  http_port_override=None,
                  https_port_override=None,
                  _unutf8=codecs.getdecoder ("utf-8"),
                  _parse_qsl=urlparse.parse_qsl,
                  _ErrorResponse=response_module.ErrorResponse,
                  _defaultdict=collections.defaultdict,
                  _reset_harakiri=utils.reset_harakiri,
                  _dict=dict,
                  _dict_items=utils.iter_dict_items,
                  _last=operator.itemgetter (-1),
                  _list=list,
                  _len=len,
                  _py2=six.PY2):
        _reset_harakiri()
        
        self._ErrorResponse = _ErrorResponse
        
        self.env          = env
        self.method       = env["REQUEST_METHOD"]
        self.input_stream = env["wsgi.input"]
        
        Q = _defaultdict (_list)
        
        raw_uri = env.get ("REQUEST_URI")
        if raw_uri is None:
            raw_uri = env.get ("RAW_URI")
        
        query_needs_decoding = _py2
        
        if raw_uri is not None:
            # we prefer to parse the unescaped REQUEST_URI/RAW_URI instead of WSGI's PATH_INFO
            
            if _py2:
                try:
                    uri = _unutf8 (raw_uri, "strict") [0]
                    self.mountpoint = mountpoint = _unutf8 (env.get ("SCRIPT_NAME", b"")) [0]
                except UnicodeDecodeError:
                    raise _ErrorResponse (400, "Path is not valid UTF-8")
            else:
                uri = raw_uri
                self.mountpoint = mountpoint = env.get ("SCRIPT_NAME", "")
            
            parts = uri.split ("?", 1)
            if _len (parts) > 1:
                query = parts[1]
            else:
                query = ""
            path = parts[0]
            
            if mountpoint:
                if path.startswith (mountpoint):
                    self.path_info = path [_len (mountpoint):]
                else:
                    # TODO: keep decoding raw_uri until we get the SCRIPT_NAME?
                    raise _ErrorResponse (500, "Mountpoint mismatch")
            else:
                self.path_info = path
            
            query_needs_decoding = 0
        
        else:
            if _py2:
                try:
                    self.mountpoint = _unutf8 (env.get ("SCRIPT_NAME", b"")) [0]
                    self.path_info = _unutf8 (env["PATH_INFO"], "strict") [0]
                except UnicodeDecodeError:
                    raise _ErrorResponse (400, "Path is not valid UTF-8")
            
            else:
                self.mountpoint = env.get ("SCRIPT_NAME", "")
                self.path_info = env["PATH_INFO"]
            
            query = env["QUERY_STRING"]
        
        if query_needs_decoding:
            try:
                for k, v in _parse_qsl (query, keep_blank_values=1):
                    Q[ _unutf8 (k)[0] ].append (_unutf8 (v)[0])
            except UnicodeDecodeError:
                raise _ErrorResponse (400, "Query is not valid UTF-8")
        
        else:
            for k, v in _parse_qsl (query, keep_blank_values=1):
                Q[k].append (v)
        
        q = self.query_list = _dict (Q)
        self.query = { k: _last (v) for k, v in _dict_items (q) }
        
        self.headers = _defaultdict (_list)
        input_length = self.env.get ("CONTENT_LENGTH")
        if input_length:
            try:
                input_length = int (input_length.strip ())
            except Exception:
                raise _ErrorResponse (400, "Invalid Content-Length")
        
        self.input_length = input_length or None
        self.input_type   = self.env.get ("CONTENT_TYPE", "").split (";")[0].strip ().lower ()
        
        if env.get ("HTTPS") == "on":
            self.scheme = "https"
        elif trust_forwarded_headers:
            self.scheme = (env.get("HTTP_X_FORWARDED_PROTO") or env.get("wsgi.url_scheme", "http")).lower()
        else:
            self.scheme = env.get("wsgi.url_scheme", "http").lower()
        
        if self.scheme == "https":
            port_override = https_port_override
        else:
            port_override = http_port_override
        
        authority = (env.get ("HTTP_HOST") or env["SERVER_NAME"]).split (":", 1)
        if authority[1:]:
            host, port = authority
        else:
            host = authority[0]
            port = None
        
        if trust_forwarded_headers:
            self.port = port_override or env.get ("HTTP_X_FORWARDED_PORT") or env.get ("SERVER_PORT") or port
            self.remote_address = env.get ("HTTP_X_FORWARDED_FOR") or env.get ("REMOTE_ADDR")
        else:
            self.port = port_override or env.get ("SERVER_PORT") or port
            self.remote_address = env.get ("REMOTE_ADDR")
        
        if (self.scheme == "https" and self.port == "443") or (self.scheme == "http" and self.port == "80"):
            authority = host
        else:
            authority = "%s:%s" % (host, self.port)
            
        self.host     = authority
        self.path     = self.mountpoint + self.path_info
        self.base_url = "%s://%s%s" % (self.scheme, authority, self.mountpoint)
        self.url      = self.base_url + self.path_info
        
        #print("HTTP_HOST=%s, SERVER_NAME=%s, SERVER_PORT=%s, HTTP_X_FORWARDED_PORT=%s. base_url=%s" % (env.get("HTTP_HOST"), env.get("SERVER_NAME"), env.get("SERVER_PORT"), env.get ("HTTP_X_FORWARDED_PORT"), self.base_url))
    
    def read_body (self,
                   temporary_directory=None,
                   hash_algorithm=None,
                   max_memory_size=4096,
                   max_body_size=2**20,
                   max_body_params_size=2**15,
                   _UploadedFile=files_module.UploadedFile,
                   _unutf8=codecs.getdecoder ("utf-8"),
                   _parse_qsl=urlparse.parse_qsl,
                   _defaultdict=collections.defaultdict,
                   _dict_items=utils.iter_dict_items,
                   _last=operator.itemgetter (-1),
                   _FieldStorage=files_module.FieldStorage,
                   _to_unicode=utils.to_unicode,
                   _reset_harakiri=utils.reset_harakiri,
                   _list=list,
                   _dict=dict,
                   _len=len,
                   _getattr=getattr,
                   _py2=six.PY2):
        
        if self._body_read:
            raise ProgrammingError (".read_body() called twice.")
        
        self._body_read = True
        input_length = self.input_length
        if input_length:
            if input_length > max_body_size:
                raise self.fail (413)
            
            content_type = self.env.get ("CONTENT_TYPE", "")
            if content_type:
                parts = content_type.split (";", 1)
                content_type = "%s; %s" % (parts[0].strip().lower (), "".join (parts[1:]))
            else:
                content_type = "application/x-www-form-urlencoded"
            
            if content_type == "application/x-www-form-urlencoded":
                if input_length > max_body_params_size:
                    raise self.fail (413)
                
                body = self.input_stream.read (self.input_length)
                while _len (body) < input_length:
                    body += self.input_stream.read (input_length - _len (body))
                
                Q = _defaultdict (_list)
                if _py2:
                    for k, v in _parse_qsl (body, keep_blank_values=1):
                        Q[ _unutf8 (k)[0] ].append (_unutf8 (v)[0])
                else:
                    for k, v in _parse_qsl (body, keep_blank_values=1):
                        Q[k].append (v)
                
                q = self._posted_data_list = _dict (Q)
                self._posted_data = { k: _last (v) for k, v in _dict_items (q) }
                self._posted_files = {}
            
            else:
                is_multipart = content_type[:10] == "multipart/"
                
                if is_multipart:
                    content_disposition = self.env.get ("HTTP_CONTENT_DISPOSITION", "")
                else:
                    # filename needs to be set for FieldStorage to treat file as binary data
                    content_disposition = self.env.get ("HTTP_CONTENT_DISPOSITION", "attachment; filename=\"\"")
                
                fs = _FieldStorage (fp=self.env["wsgi.input"],
                                    temporary_directory=temporary_directory,
                                    hash_algorithm=hash_algorithm,
                                    max_memory_size=max_memory_size,
                                    headers={ "content-type":        content_type,
                                              "content-length":      "%s" % input_length,
                                              "content-disposition": content_disposition },
                                    environ={ "REQUEST_METHOD": "POST" })
                
                self._field_storage = fs # needs to be here to keep the reference alive, otherwise it'll close its files
                
                Q = _defaultdict (_list)
                F = _defaultdict (_list)
                
                if is_multipart:
                    fslist = fs.list or ()
                else:
                    fslist = (fs,)
                    fs._nw_as_file = 1
                
                for it in fslist:
                    if it.filename or _getattr (it, "_nw_as_file", None):
                        F[it.name].append (_UploadedFile (fileobj=it.file,
                                                          name=_to_unicode (it.name, _mode="replace"),
                                                          filename=_to_unicode (it.filename or "", _mode="replace"),
                                                          temporary_filename=_getattr (it.file, "name", None),
                                                          checksum=it.get_checksum(),
                                                          content_type=_to_unicode (it.type),
                                                          content_length=it.get_actual_length() if it.length == -1 else it.length))
                    else:
                        Q[it.name].append (it.value)
                
                q = self._posted_data_list = _dict (Q)
                self._posted_data = { k: _last (v) for k, v in _dict_items (q) }
                self._posted_files = F
            
            _reset_harakiri()
        else:
            self._posted_files = self._posted_data = self._posted_data_list = {}
    
    def discard_body (self):
        if not self._body_read:
            self._body_read = True
            
            remaining = self.input_length
            rest = 1
            _len = len
            _min = min
            while remaining and rest:
                rest = self.input_stream.read (_min (remaining, 16384))
                remaining -= _len (rest)
    
    @property
    def post (self):
        if not self._body_read:
            raise ProgrammingError (".read_body() hasn't been called yet.")
        return self._posted_data
    
    @property
    def post_list (self):
        if not self._body_read:
            raise ProgrammingError (".read_body() hasn't been called yet.")
        return self._posted_data_list
    
    @property
    def files (self):
        if not self._body_read:
            raise ProgrammingError (".read_body() hasn't been called yet.")
        return self._posted_files
    
    def fail (self, status_code, error_message=""):
        """
        :rtype: ErrorResponse
        """
        return self._ErrorResponse (status_code, error_message, "text/html" in self.env.get("ACCEPT", ""))
    
    def redirect (self, url, permanent=False, add_mountpoint=True):
        if add_mountpoint and url.startswith ("/"):
            url = self.mountpoint + url
        
        rs = self._ErrorResponse (301 if permanent else 302)
        rs.set_header ("LOCATION", url)
        return rs
    
    # TODO: HEAD support?
    
    def respond (self, status_code, error_message="", _Response=response_module.Response):
        return _Response (status_code, error_message=error_message)
    
    def respond_with_bytes (self, data,
                            status_code=200,
                            error_message="",
                            mimetype="application/octet-stream",
                            _Response=response_module.Response,
                            _to_bytes=utils.to_bytes):
        rs = self.response = _Response (status_code, error_message)
        rs.body = _to_bytes (data)
        rs.set_header ("CONTENT-TYPE", mimetype)
        rs.set_header ("CONTENT-LENGTH", len(rs.body))
        
        return rs
    
    def respond_with_text (self, data,
                           status_code=200,
                           error_message="",
                           mimetype="text/plain",
                           charset="utf-8",
                           _Response=response_module.Response,
                           _getattr=getattr):
        self.status_code = status_code
        
        encode = _getattr (data, "encode", None)
        if encode:
            data = encode (charset)
        
        rs = self.response = _Response (status_code, error_message)
        rs.body = data
        rs.set_header ("CONTENT-TYPE", "%s; charset=%s" % (mimetype, charset))
        rs.set_header ("CONTENT-LENGTH", len(data))
        
        return rs
    
    def respond_with_html (self, data,
                           status_code=200,
                           error_message="",
                           mimetype="text/html",
                           charset="utf-8"):
        return self.respond_with_text (data,
                                       status_code=status_code,
                                       error_message=error_message,
                                       mimetype=mimetype,
                                       charset=charset)
    
    def respond_with_json (self, data,
                           status_code=200,
                           error_message="",
                           mimetype="application/json",
                           _encoder=json.JSONEncoder (ensure_ascii=True)):
        return self.respond_with_text (_encoder.encode (data),
                                       status_code=status_code,
                                       error_message=error_message,
                                       mimetype=mimetype,
                                       charset="utf-8")
    
    def respond_with_file (self, fileobj,
                           status_code=200,
                           error_message="",
                           chunk_size=8192,
                           mimetype="application/octet-stream",
                           _Response=response_module.Response,
                           _file_iter=files_module.file_iter):
        
        rs = self.response = _Response (status_code, error_message)
        wrapper = self.env.get ("wsgi.file_wrapper")
        if wrapper:
            rs.stream = wrapper (fileobj)
        else:
            rs.stream = _file_iter (fileobj, chunk_size=chunk_size)
        rs.set_header ("CONTENT-TYPE", mimetype)
        
        return rs
