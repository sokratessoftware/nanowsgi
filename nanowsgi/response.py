# coding=utf-8
from __future__ import unicode_literals

import sys
import codecs
import collections

from six.moves.urllib import parse as urlparse

from . import utils

HTTP_STATUSES = { 200: "OK",
                  201: "Created",
                  202: "Accepted",
                  203: "Non-Authoritative Information",
                  204: "No Content",
                  205: "Reset Content",
                  206: "Partial Content",
                  207: "Multi-Status",
                  208: "Already Reported",
                  226: "IM Used",
                  
                  300: "Multiple Choices",
                  301: "Moved Permanently",
                  302: "Found",
                  303: "See Other",
                  304: "Not Modified",
                  305: "Use Proxy",
                  306: "Switch Proxy",
                  307: "Temporary Redirect",
                  308: "Permanent Redirect",
                  
                  400: "Bad Request",
                  401: "Unauthorized",
                  402: "Payment Required",
                  403: "Forbidden",
                  404: "Not Found",
                  405: "Method Not Allowed",
                  406: "Not Acceptable",
                  407: "Proxy Authentication Required",
                  408: "Request Timeout",
                  409: "Conflict",
                  410: "Gone",
                  411: "Length Required",
                  412: "Precondition Failed",
                  413: "Request Entity Too Large",
                  414: "Request-URI Too Long",
                  415: "Unsupported Media Type",
                  416: "Requested Range Not Satisfiable",
                  417: "Expectation Failed",
                  418: "No, This is Patrick",
                  419: "Authentication Timeout",
                  420: "Enhance Your Calm",
                  422: "Unprocessable Entity",
                  423: "Locked",
                  424: "Failed Dependency",
                  426: "Upgrade Required",
                  428: "Precondition Required",
                  429: "Too Many Requests",
                  431: "Request Header Fields Too Large",
                  451: "Unavailable For Legal Reasons",
                  
                  500: "Internal Server Error",
                  501: "Not Implemented",
                  502: "Bad Geteway",
                  503: "Service Unavailable",
                  504: "Gateway Timeout",
                  505: "HTTP Version Not Supported",
                  506: "Variant Also Negotiates",
                  507: "Insufficient Storage",
                  508: "Loop Detected",
                  509: "Bandwidth Limit Exceeded",
                  510: "Not Extended",
                  511: "Network Authentication Required" }

if sys.version_info.major <= 2:
    def prep_status_line (status_code, custom_text="",
                          STATUSES=HTTP_STATUSES,
                          _filter=filter,
                          _ascii=codecs.getencoder ("utf-8")):
        status_text = ": ".join (_filter (None, (STATUSES.get (status_code, ""), custom_text)))
        return _ascii ("%s %s" % (status_code, status_text)) [0]
else:
    def prep_status_line (status_code, custom_text="",
                          STATUSES=HTTP_STATUSES,
                          _filter=filter):
        status_text = ": ".join (_filter (None, (STATUSES.get (status_code, ""), custom_text)))
        return "%s %s" % (status_code, status_text)

class Response (object):
    """
    Base response class.
    
    There's no need to reference it directly, the `Request` class has methods for creating responses.
    """
    
    body = None
    """A binary string containing data to be returned as response body."""
    
    stream = None
    """A generator or other iterator returning binary strings to be returned as response body."""
    
    def __init__ (self,
                  status_code=200,
                  error_message="",
                  _list=list,
                  _defaultdict=collections.defaultdict):
        self.status_code = status_code
        self.error_message = error_message
        self.headers = _defaultdict (_list)
        if error_message:
            self.headers[b"X-NANOWSGI-ERROR"] = (error_message,)
    
    def set_header (self, name, value):
        name = name.upper ()
        self.headers[name] = [value]
        return self
    
    def with_header (self, name, value):
        name = name.upper ()
        self.headers[name].append (value)
        return self
    
    def with_content_disposition (self, filename, disposition="attachment",
                                  _prepare_filename=utils.make_ascii_filename,
                                  _to_unicode=utils.to_unicode,
                                  _to_utf8=codecs.getencoder ("utf-8"),
                                  _quote=urlparse.quote):
        filename = _to_unicode (filename)
        fn_ascii = _to_unicode (_prepare_filename (filename))
        fn_url   = _to_unicode (_quote (_to_utf8 (filename) [0]))
        
        if fn_ascii != fn_url:
            return self.set_header ("CONTENT-DISPOSITION", "%s; filename=%s; filename*=utf-8''%s" % (disposition, fn_ascii, fn_url))
        else:
            return self.set_header ("CONTENT-DISPOSITION", "%s; filename=%s" % (disposition, fn_ascii))
    
    def _prep_headers (self,
                       _header_dict_to_list=utils.header_dict_to_list):
        return _header_dict_to_list (self.headers, self.status_code)
    
    @property
    def status_line (self,
                     _prep_status_line=prep_status_line):
        return _prep_status_line (self.status_code)
    
    def _send (self, start_response):
        start_response (self.status_line, self._prep_headers ())
        
        if self.stream:
            return self.stream
        
        elif self.body:
            return self.body,
        
        return ()

CACHED_ERROR_PAGES = {}

class ErrorResponse (Exception, Response):
    def __init__ (self, status_code, error_message="", html=0):
        Response.__init__ (self, status_code, error_message)
        Exception.__init__ (self, self.status_line)
        
        key = (status_code, error_message)
        body = CACHED_ERROR_PAGES.get(key)
        if body is None:
            body = CACHED_ERROR_PAGES[key] = utils.to_bytes ("<html><head><title>Error %s</title><body><h1>Error %s</h1><p>%s</p></body></head></html>" % (status_code, status_code, error_message))
        
        self.body = body
        self.set_header("CONTENT-TYPE", "text/html")
        self.set_header("CONTENT-LENGTH", "%s" % len(body))
