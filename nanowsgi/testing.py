import sys

import wsgiref.validate
import wsgiref.handlers

from six import BytesIO

from . import utils

class TestHandler (wsgiref.handlers.BaseHandler):
    """
    A simple WSGI handler for testing WSGI applications.
    
    Usage:
        with TestHandler (application, path="/") as handler:
            assert handler.status_code == 200
    """
    
    status_code = None
    """Status code returned by WSGI application."""
    
    headers = None
    """Dictionary of returned headers."""
    
    response_body = None
    """Binary string containing response body."""
    
    def __init__ (self, application, method="GET", path="/", query="", content_type=None, body=None, file_name=None, source_addr="127.0.0.1"):
        """
        :param application:  WSGI application callable 
        :param method:       HTTP method in uppercase
        :param path:         Request path
        :param query:        Query string without leading question mark
        :param body:         Request body
        :param file_name:    Uploaded file name
        :param content_type: Uploaded file MIME type
        """
        self.application = application
        self.inbuf = BytesIO ()
        self.outbuf = BytesIO ()
        self.method = method
        self.path = path
        self.query = query
        self.content_type = content_type
        self.file_name = file_name
        self.source_addr = source_addr
        
        self._write = self.outbuf.write
        
        if body:
            self.inbuf.write (utils.to_bytes (body))
            self.inbuf.seek (0)
            self.content_length = len (body)
        else:
            self.content_length = None
    
    def close (self):
        # we don't want run() to automatically cleanup response state, we'll do that in __exit__ instead
        pass
    
    def send_headers (self):
        self.cleanup_headers ()
        # we don't want headers to end up in the output buffer
    
    def __enter__ (self):
        self.run (self.application)
        self.status_code = int (self.status.split () [0])
        self.response_body = self.outbuf.getvalue ()
        return self
    
    def __exit__ (self, exc_type, exc_val, exc_tb, _close=wsgiref.handlers.BaseHandler.close):
        _close (self)
    
    def _flush (self):
        pass
    
    def get_stdin (self):
        return self.inbuf
    
    def get_stderr (self):
        return sys.stderr
    
    def add_cgi_vars (self, _to_str=utils.to_str):
        env = self.environ
        
        env["REQUEST_METHOD"] = _to_str (self.method)
        env["REQUEST_URI"]    = _to_str (self.path) + "?" + _to_str (self.query)
        env["PATH_INFO"]      = _to_str (self.path)
        env["QUERY_STRING"]   = _to_str (self.query)
        env["REMOTE_ADDR"]    = _to_str (self.source_addr)
        
        if self.content_type:
            env["CONTENT_TYPE"] = _to_str (self.content_type)
        
        if self.content_length is not None:
            env["CONTENT_LENGTH"] = _to_str ("%s" % self.content_length)
        
        if self.file_name:
            env["HTTP_CONTENT_DISPOSITION"] = _to_str ("attachment; filename=\"%s\"" % self.file_name.replace ('"', ""))
    
    os_environ = { "SERVER_NAME":     "dziupla.test",
                   "SERVER_PORT":     "80",
                   "SERVER_PROTOCOL": "HTTP/1.0",
                   "SCRIPT_NAME":     "" }
