# coding=utf-8
from __future__ import unicode_literals

import sys
import codecs, unicodedata
import operator

def to_bytes (val,
              _encode=codecs.getencoder ("utf-8"),
              _mode="strict",
              _isinstance=isinstance,
              _bytes=bytes):
    """Convert input `val` to binary string (bytes in python3, str in python2)."""
    
    if _isinstance (val, _bytes):
        return val
    else:
        return _encode ("%s" % val, _mode) [0]

if sys.version_info.major > 2:
    def to_str (val,
                _decode=codecs.getdecoder ("utf-8"),
                _mode="strict",
                _isinstance=isinstance,
                _str=str,
                _bytes=bytes):
        """Convert input `val` to str."""
        
        if _isinstance (val, _str):
            return val
        elif _isinstance (val, _bytes):
            return _decode (val, _mode) [0]
        return "%s" % val
    
    to_unicode = to_str
    
    iter_dict_items = operator.methodcaller ("items")
    
    maketrans = bytes.maketrans
else:
    def to_str (val,
                _encode=codecs.getencoder ("utf-8"),
                _mode="strict",
                _isinstance=isinstance,
                _unicode=unicode):
        """Convert input `val` to str."""
        
        if _isinstance (val, _unicode):
            return _encode (val, _mode) [0]
        # TODO: consider converting to unicode instead and encoding
        return b"%s" % val
    
    def to_unicode (val,
                    _decode=codecs.getdecoder ("utf-8"),
                    _mode="strict",
                    _isinstance=isinstance,
                    _bytes=bytes,
                    _unicode=type ("")):
        """Convert input `val` to unicode string (str in python3, unicode in python2)."""
        
        if _isinstance (val, _unicode):
            return val
        elif _isinstance (val, _bytes):
            return _decode (val, _mode) [0]
        return "%s" % val
    
    iter_dict_items = operator.methodcaller ("viewitems")
    
    import string
    maketrans = string.maketrans

def make_ascii_filename (unicode_filename,
                         _ascii=codecs.getencoder ("ascii"),
                         _normalize=unicodedata.normalize,
                         _translation_table=maketrans (b" /\t\f\v\n\0,;*():<=>?@[]{}", b"_" * 22),
                         _deletion_table=b"\x01\x02\x03\x04\x05\x06\x07\x08\x0D\x0E\x0F\x00\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F\"'"):
    return _ascii (_normalize ("NFKD", unicode_filename)
                   .replace ("ł", "l")
                   .replace ("Ł", "L"), "ignore") [0]\
            .translate (_translation_table, _deletion_table)\
            .replace (b"___", b"_")\
            .replace (b"__", b"_")\
            .replace (b"_.", b".")

if sys.version_info.major > 2:
    def header_dict_to_list (headers,
                             status_code=200,
                             _isinstance=isinstance,
                             _str=str,
                             _bytes=bytes,
                             _dict_items=iter_dict_items,
                             _decode=codecs.getdecoder ("ascii")):
        """Encoding a python dict with unicode keys and values with sequences of unicode strings into a header list accepted by WSGI."""
        
        header_list = []
        header_list_append = header_list.append
        
        for k, vs in _dict_items (headers):
            for v in vs:
                if _isinstance (k, _bytes):
                    k = _decode (k, "strict") [0]
                elif not _isinstance (k, _str):
                    k = "%s" % k
                
                if _isinstance (v, _bytes):
                    v = _decode (v, "xmlcharrefreplace") [0]
                elif not _isinstance (v, _str):
                    v = "%s" % v
                
                k = k.replace ("\n", "").replace ("\r", "")
                v = v.replace ("\n", " ").replace ("\r", " ")
                
                header_list_append ((k, v))
        
        if "CONTENT-TYPE" not in headers and status_code not in (204, 304):
            header_list_append (("CONTENT-TYPE", "text/plain"))
        
        return header_list
else:
    def header_dict_to_list (headers,
                             status_code=200,
                             _isinstance=isinstance,
                             _str=str,
                             _unicode=unicode,
                             _dict_items=iter_dict_items,
                             _encode=codecs.getencoder ("ascii")):
        """Encoding a python dict with unicode keys and values with sequences of unicode strings into a header list accepted by WSGI."""
        
        header_list = []
        header_list_append = header_list.append
        
        for k, vs in _dict_items (headers):
            for v in vs:
                if _isinstance (k, _unicode):
                    k = _encode (k, "strict") [0]
                elif not _isinstance (k, _str):
                    k = "%s" % k
                
                if _isinstance (v, _unicode):
                    v = _encode (v, "xmlcharrefreplace") [0]
                elif not _isinstance (v, _str):
                    v = "%s" % v
                
                k = k.replace (b"\n", b"").replace (b"\r", b"")
                v = v.replace (b"\n", b" ").replace (b"\r", b" ")
                
                header_list_append ((k, v))
        
        if "CONTENT-TYPE" not in headers and status_code not in (204, 304):
            header_list_append ((b"CONTENT-TYPE", b"text/plain"))
        
        return header_list

TIMEOUT_QUANTUM = 20

try:
    from uwsgi import set_user_harakiri
    def reset_harakiri(timeout=TIMEOUT_QUANTUM):
        set_user_harakiri(timeout)
except ImportError:
    def reset_harakiri(timeout=TIMEOUT_QUANTUM):
        pass
