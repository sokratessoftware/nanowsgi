# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name = "nanowsgi",
    version = "0.2.7.0",
    author = 'SOKRATES-software',
    author_email = "sokrates@sokrates.pl",
    description = "nanowsgi :: SOKRATES-software minimal wsgi framework",
    license = "Proprietary",
    url = "http://www.sokrates.pl",
    package_dir = {"nanowsgi": "nanowsgi"},
    packages = ["nanowsgi"],
    requires = ["six"],
)
